#include <iostream>
#include "fcgio.h"
#include <map>
#include <stdlib.h>
#include <cerrno>
#include <cstring>

using namespace std;

const int kMaxFiles = 1000;

void returnScene(string scene) {
    auto outputString = string("<html><style>html, body {overflow: hidden;}</style><body style=\"background-color:white\"><div><center><div id=\"moduleStatusString\" style=\"color: red; font-size: 30px;\">Downloading...</div><canvas id=\"canvas\"></canvas><br><br>Controls: WSAD + E, Mouse Wheel - Change Color<br><br><input type=\"text\" id=\"serializedSceneTextField\"><br><button onclick=\"copySceneToClipboard()\">Save</button></center></div><script type=\"text/javascript\">var cubeArtProjectSerializedScene = \"");    
    outputString += scene;
    outputString += string("\"</script><script type=\"text/javascript\" src=\"loader.js\"></script><script type=\"text/javascript\" src=\"cubeArtProjectHelpers.js\"></script><script type=\"text/javascript\" src=\"main.js\"></script></body></html>");    
    
    cout << "Content-type: text/html\r\n"
         << "\r\n"
         << outputString;    
}

int loadCounter() {
    auto filePath = "capscs/numberOfRecords";
    FILE *file = fopen(filePath, "r");
    if (file != nullptr) {
        fseek(file, 0, SEEK_END);
        size_t size = ftell(file);
        auto numberOfRecordsC = (char *)calloc(size, sizeof(char*));
        rewind(file);
        fread(numberOfRecordsC, sizeof(char), size, file);
        auto numberOfRecordsString = string(numberOfRecordsC);
        delete[] numberOfRecordsC;
        fclose(file);   
        return stoi(numberOfRecordsString);
    }
    
    {
                cout << "Content-type: text/html\r\n"
                     << "\r\n"
                     << "can't write file: " << std::strerror(errno);
    }    
    
    return 0;
}

void saveCounter(int counter) {
    auto filePath = "capscs/numberOfRecords";
    remove("capscs/numberOfRecords");
    FILE *fp = fopen(filePath, "w");
    if (fp) {
        fprintf(fp,"%d", counter);
        fclose(fp);
    }       
    if (errno) {
                cout << "Content-type: text/html\r\n"
                     << "\r\n"
                     << "can't write file: " << std::strerror(errno);
    }       
}

void returnSerializedSceneForKey(string key) {
    auto filePath = key;
    filePath = "capscs/" + key + ".capsc";
    FILE *file = fopen(filePath.c_str(), "r");
    if (file == nullptr) {
        returnScene(string("{}"));
    }
    else {
        fseek(file, 0, SEEK_END);
        size_t size = ftell(file);
        auto sceneC = (char *)calloc(size, sizeof(char*));
        rewind(file);
        fread(sceneC, sizeof(char), size, file);
        auto scene = string(sceneC);
        delete[] sceneC;        
        returnScene(scene);
        fclose(file);
    }
}

void save(string key, string value) {
    auto filePath = key;
    filePath = "capscs/" + key + ".capsc";
    FILE *fp = fopen(filePath.c_str(), "w");
    if (fp == nullptr)
    {
                cout << "Content-type: text/html\r\n"
                     << "\r\n"
                     << "can't write file: " << std::strerror(errno);
    }
    else
    {
        fprintf(fp,"%s", value.c_str());
        fclose(fp);
    }    
}

int main(void) {
    streambuf * cin_streambuf  = cin.rdbuf();
    streambuf * cout_streambuf = cout.rdbuf();
    streambuf * cerr_streambuf = cerr.rdbuf();

    FCGX_Request request;

    FCGX_Init();
    FCGX_InitRequest(&request, 0, 0);
    
    while (FCGX_Accept_r(&request) == 0) {
        
        fcgi_streambuf cin_fcgi_streambuf(request.in);
        fcgi_streambuf cout_fcgi_streambuf(request.out);
        fcgi_streambuf cerr_fcgi_streambuf(request.err);

        cin.rdbuf(&cin_fcgi_streambuf);
        cout.rdbuf(&cout_fcgi_streambuf);
        cerr.rdbuf(&cerr_fcgi_streambuf);

        auto queryString = FCGX_GetParam("QUERY_STRING", request.envp);
        auto contentLengthString = FCGX_GetParam("CONTENT_LENGTH", request.envp);
        
        if (queryString != nullptr) {
            auto query = string(queryString);
            if (
                query == string("write") && 
                contentLengthString != nullptr
            ) {
                auto contentLength = strtol(contentLengthString,
                                &contentLengthString,
                                10);
                
                if (contentLength > 512 * 1024) {
                    return 1;
                }
                
                auto postContent = (char *)calloc(contentLength, sizeof(char*));
                cin.read(postContent, contentLength);
                auto value = string(postContent);
                free(postContent);
                
                if (value.length() < 3) {
                    return 1;
                }

                auto numberOfRecords = loadCounter();
                numberOfRecords += 1;
                auto key = to_string(numberOfRecords);   
                save(key, value);    
                
                if (numberOfRecords > kMaxFiles) {
                    numberOfRecords = 0;
                }
                
                saveCounter(numberOfRecords);
                
                cout << "Content-type: text/html\r\n"
                     << "\r\n"
                     << "saved: "
                     << value
                     << "<br>id: "
                     << key;
            }
            else {
                returnSerializedSceneForKey(query);
            }
        }
        else {
            returnScene(string("{}"));            
        }
    }
    cin.rdbuf(cin_streambuf);
    cout.rdbuf(cout_streambuf);
    cerr.rdbuf(cerr_streambuf);

    return 0;
}
