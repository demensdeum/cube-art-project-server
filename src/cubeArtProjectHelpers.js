function copySceneToClipboard() {

    var copyText = document.getElementById("serializedSceneTextField");
    
    let xhr = new XMLHttpRequest(); 
    let url = "cubeArtProjectServer.fcgi?write"; 
    xhr.open("POST", url, true); 
    xhr.setRequestHeader("Content-Type", "application/json"); 
    var data = copyText.value.replace("https://demensdeum.com/games/CubeArtProjectWEB/?scene=", "").replace("\"cubes\"","\\\"cubes\\\"");
    xhr.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            var id = this.responseText.split("id: ")[1]
            copyText.value = "http://" + window.location.hostname + "/cgi-bin/cubeArtProject/cubeArtProjectServer.fcgi?" + id
            console.log(this.responseText);
            console.log(copyText.value);
        }
    }    
    xhr.send(data);
    
}function checkFocus() {window.focus();window.setInterval(checkFocus, 1000);document.getElementById("serializedSceneTextField").value = "<Empty Scene>"};
